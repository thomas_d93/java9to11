package java11Test;

import org.junit.Test;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Unit test for simple App.
 */
public class AppTest
{

    @Test
    public void collectionsOf(){
        // .of creates immutable collections

        List list = List.of(1,2,3);
        System.out.println(list.getClass());
        System.out.println(list);

        Set set = Set.of(1, 2, 3);
        System.out.println(set.getClass());
        System.out.println(set);

        Map map = Map.of("name", "thomas");
        System.out.println(map.getClass());
        System.out.println(map);
    }

    @Test
    public void streamWhile() {
        System.out.println("takeWhile: ");
        IntStream.range(1, 10).takeWhile(x -> x < 5).forEach(System.out::println);

        System.out.println("dropWhile: ");
        IntStream.range(1, 10).dropWhile(x -> x < 5).forEach(System.out::println);

        System.out.println("iterate: ");
        IntStream.iterate(0, x -> x < 3, x -> x + 1).forEach(System.out::println);
    }

    @Test
    public void completableFuture_copyAndCompleteCopy_shallOnlyCompleteCopy() {
        CompletableFuture<String> future = new CompletableFuture<>();
        CompletableFuture<String> futureCopy = future.copy();
        futureCopy.complete("test");

        System.out.println("future completed? " + future.isDone());
        System.out.println("futureCopy completed? " + futureCopy.isDone());
    }

    @Test
    public void completableFuture_copyAndCompleteOriginal_shallCompleteBoth() {
        CompletableFuture<String> future = new CompletableFuture<>();
        CompletableFuture<String> futureCopy = future.copy();
        future.complete("test");

        System.out.println("future completed? " + future.isDone());
        System.out.println("futureCopy completed? " + futureCopy.isDone());
    }

    @Test
    public void completableFuture_timeout() {
        CompletableFuture<String> future = new CompletableFuture<>();
        future.completeOnTimeout("Future timed out", 1, TimeUnit.SECONDS);
        System.out.println(future.isDone());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(future.isDone());
    }

    @Test
    public void optional_or() {
        Optional<String> stringOptional = Optional.empty();
        System.out.println(stringOptional.or(() -> Optional.of("Test")));
        stringOptional = Optional.of("Not empty");
        System.out.println(stringOptional.or(() -> Optional.of("Test")));
    }

    @Test
    public void optional_ifPresentOrElse() {
        Optional<String> stringOptional = Optional.empty();
        stringOptional.ifPresentOrElse(System.out::println, () -> System.out.println("Optional is empty"));
        stringOptional = Optional.of("Optional is not empty");
        stringOptional.ifPresentOrElse(System.out::println, () -> System.out.println("Optional is empty"));
    }

    @Test
    public void optional_stream() {
        List<Optional<Integer>> optionalStream = List.of(Optional.of(1), Optional.of(2), Optional.empty(), Optional.of(4));
        Stream<Integer> actualStream = optionalStream.stream().flatMap(Optional::stream);
        actualStream.forEach(System.out::println);
    }

    @Test
    public void optional_get_orElseThrow() {
        //orElseThrow() acts like get()
        Optional<String> stringOptional = Optional.of("test");
//        Optional<String> stringOptional = Optional.empty();
        System.out.println(stringOptional.get());
        System.out.println(stringOptional.orElseThrow());
    }
}
